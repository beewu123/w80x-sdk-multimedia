#ifndef _CODEC_H_
#define _CODEC_H_

#define I2C_REQ     100000
#define CODEC_ADDR  0x30



void CodecInit(void);
void CodecVolume(int vol);
void CodecSetSampleRate(unsigned int sr);


#endif

