#ifndef _AUDIO_H_
#define _AUDIO_H_

/*only one MACRAO can open*/
#define USE_WAV_FILE_AS_PLAY_FTR 1
#define USE_RECORD_DATA_AS_PLAY_FTR 0
#define LOOP_CODEC_FTR  0

#define I2S_PLAY_AS_MASTER_RECORD_AS_SLAVE_FTR 0
#define CODEC_ALWAYS_AS_MASTER_FTR 1
#define CODEC_AS_MASTER (0)
#define CODEC_AS_SLAVE  (1)

#define USE_MP3_FOR_AUDIO   (1)

#define DEBUG_MP3_PLAY_FOR_AUDIO (0)

#define AUDIO_BUF_SIZE		(1024 * 2)


// fifio_size: default value (10 * 1024), must more than default value
uint8_t AudioInit(uint32_t fifo_size);

// vol: 0 ~ 100, -95dB ~ +5dB
void CodecVolume(int vol);

/* Requires users to implement the interface themselves*/
/* Function of this interface: control pin for enabling amplifier */
/* param: flag : 0 ~ disable; 1 ~ enable */
extern void UserPACtrl(uint8_t flag);

// play and record data in fifo, Obtain recording data or playback data through the following interfaces
int FifoDataLen(void);
int FifoSpaceLen(void);
int FifoRead(uint8_t *buf, uint32_t len);
int FifoWrite(uint8_t *buf, uint32_t len);
void FifoClear(void);

void PlayStart(uint32_t sr, uint8_t bit_width);
void PlayPause(void);
void PlayResume(void);
void PlayStop(void);
void RecordStart(uint32_t sr, uint8_t bit_width);
void RecordStop(void);

void audio_mp3_decode(void);


#endif

