#include "wm_include.h"
#include "string.h"
#include "wm_gpio_afsel.h"
#include "audioplay.h"
#include "fifo.h"
#include "wm_i2s.h"
#include "audio.h"
#include "codec.h"
#include "dev_ringbuf.h"
#include "pvmp3_dec.h"
#include "user_config.h"
#include "voice_buf.h"

#include "../user_config.h"

#if USE_PSRAM_AUDIO
#define MEM_MALLOC  dram_heap_malloc
#define MEM_FREE    dram_heap_free
#else
#define MEM_MALLOC  tls_mem_alloc
#define MEM_FREE    tls_mem_free
#endif


#if USE_WAV_FILE_AS_PLAY_FTR
uint8_t gplaynum = 0xFF;
uint8_t playfinishedflag = 0;
static int playedlen = 0;
// extern int get_audio_resource(int playnum, int curpos, uint8_t **playdata, int *totalplaylen);
#endif



#define TASK_MP3_STK_SIZE	512
#define TASK_MP3_PRO		32

#define USE_MALLOC_AUDIO_BUF_FTR  1
#define USE_TASK_FOR_AUDIO    0

#if USE_MALLOC_AUDIO_BUF_FTR
#if USE_TASK_FOR_AUDIO
static OS_STK *taskmp3stk = NULL;
#endif
static int16_t *audio_buffer0 = NULL;
static int16_t *audio_buffer1 = NULL;
//static int16_t *audio_rbuffer0 = NULL;
//static int16_t *audio_rbuffer1 = NULL;

#else
#if USE_TASK_FOR_AUDIO
static OS_STK taskmp3stk[TASK_MP3_STK_SIZE];
#endif
static int16_t audio_buffer0[AUDIO_BUF_SIZE];
static int16_t *audio_buffer1 = audio_buffer0 + AUDIO_BUF_SIZE / 2;
#endif

static tls_os_sem_t *audio_sem = NULL;

#if USE_MP3_FOR_AUDIO
dev_ringbuf_t dma_ringbuf;
#endif

static int32_t gsSampRate = 8000;
static int32_t gsBItWidth = 16;
static int32_t gsOutputSamps = AUDIO_BUF_SIZE/2;
//static int32_t gsOutputSamps = 12816;
static int32_t gsChans = 0;

static uint8_t gVolume = 0;
static uint8_t gOutFormat = 0;
static uint8_t gAudioFreeBuff = 0;
#define gMode_PLAY_START 	0
#define gMode_RECORD_START	1
#define gMode_PLAY_RECORD_STOP 	2

#if USE_WAV_FILE_AS_PLAY_FTR	
static uint8_t gMode = gMode_PLAY_RECORD_STOP;
#else
static uint8_t gMode = gMode_PLAY_START;
#endif


#define ENABLE_PA_2_PLAY	1
#define DISABLE_PA_2_STOP_PLAY	0
static wm_dma_handler_type dma_tx;
static wm_dma_handler_type dma_rx;

extern void wm_i2s_tx_enable(bool bl);
extern void wm_i2s_rx_enable(bool bl);
static void I2sRecordInit(int32_t sr, uint32_t bit_width, int32_t inputSamps, int16_t *buf0, int16_t *buf1);
static void I2sPlayInit(int32_t sr, int32_t nChans, uint32_t bit_width, int32_t outputSamps, int16_t *buf0, int16_t *buf1);
static int32_t AudioCallback(int16_t *samples);

static u8 *g_mp3_buf = NULL;

int get_audio_resource(int playnum, int curpos, uint8_t **playdata, int *totalplaylen)
{
    // printf("---> %d %d\n", playnum, curpos);
	switch (playnum)
	{
	case 1:
    {
        u32 len = sizeof(voice_buf);
        if(g_mp3_buf == NULL){
            printf("---> len:%d\n",len);
            g_mp3_buf = MEM_MALLOC(len);
            if(g_mp3_buf == NULL){
                printf("---> mp3_buf malloc fail!\n");
                return -1;
            }
            memcpy(g_mp3_buf, voice_buf, len);
        }
		*playdata = &g_mp3_buf[curpos];
		*totalplaylen = len;
		break;
    }

	default:
	{
		return -1;
	}
	}
	return 0;
}

void PlayStart(uint32_t sr, uint8_t bit_width)
{
    if(sr != 8000 && sr != 16000 && sr != 32000 &&
        sr != 11025 && sr != 22050 && sr != 44100 &&
        sr != 12000 && sr != 24000 && sr != 48000)
    {
        wm_printf("samplerate error!\n");
        return;
    }
    if(bit_width != 8 && bit_width != 16 && bit_width != 24 && bit_width != 32)
    {
        wm_printf("bit_width error!\n");
        return;
    }
    if(sr == 8000 && bit_width == 8)
    {
        wm_printf("not support 8K 8Bit record!\n");
        return;
    }
    gsSampRate = sr;
    gsBItWidth = bit_width;
    gMode = 0;
    gAudioFreeBuff = 0;
#if USE_WAV_FILE_AS_PLAY_FTR	
	playfinishedflag = 0;
#endif	
//    FifoClear();
    CodecSetSampleRate(gsSampRate);
#if CODEC_ALWAYS_AS_MASTER_FTR		
#else
	CodecWorkModeSwitch(CODEC_AS_SLAVE);
#endif
	memset(audio_buffer0, 0, AUDIO_BUF_SIZE);
	memset(audio_buffer1, 0, AUDIO_BUF_SIZE);	
	I2sPlayInit(gsSampRate, gsChans, gsBItWidth, gsOutputSamps, audio_buffer0, audio_buffer1);
    UserPACtrl(1);
}

void PlayPause(void)
{
    UserPACtrl(0);
    wm_i2s_tx_enable(0);
}

void PlayResume(void)
{
    UserPACtrl(1);
    wm_i2s_tx_enable(1);
}

void PlayStop(void)
{
#if USE_WAV_FILE_AS_PLAY_FTR
	while(!playfinishedflag)
	{
		tls_os_time_delay(1);
	}
#endif	

    UserPACtrl(0);
    wm_i2s_tx_enable(0);
    gMode = 2;
#if USE_WAV_FILE_AS_PLAY_FTR
    gplaynum = 0xFF;
#endif	
}

void RecordStart(uint32_t sr, uint8_t bit_width)
{
    if(sr != 8000 && sr != 16000 && sr != 32000 &&
        sr != 11025 && sr != 22050 && sr != 44100 &&
        sr != 12000 && sr != 24000 && sr != 48000)
    {
        wm_printf("samplerate error!\n");
        return;
    }
    if(bit_width != 8 && bit_width != 16 && bit_width != 24 && bit_width != 32)
    {
        wm_printf("bit_width error!\n");
        return;
    }
    if(sr == 8000 && bit_width == 8)
    {
        wm_printf("not support 8K 8Bit record!\n");
        return;
    }
    gsSampRate = sr;
    gsBItWidth = bit_width;
    gMode = 1;
    gAudioFreeBuff = 0;
    UserPACtrl(0);
    FifoClear();
    CodecSetSampleRate(gsSampRate);
#if CODEC_ALWAYS_AS_MASTER_FTR		
#else
#if I2S_PLAY_AS_MASTER_RECORD_AS_SLAVE_FTR
	CodecWorkModeSwitch(CODEC_AS_MASTER);
#else
	CodecWorkModeSwitch(CODEC_AS_SLAVE);
#endif
#endif
    I2sRecordInit(gsSampRate, gsBItWidth, AUDIO_BUF_SIZE/2, audio_buffer0, audio_buffer1);
}
#define RECORD_CLEAR_FTR 1

void RecordStop(void)
{
	int retrycnt = 0;

	gMode = 2;
	if (tls_bitband_read(HR_I2S_CTRL, 2))
	{
	    wm_i2s_rx_enable(0);
#if RECORD_CLEAR_FTR	
		while((gAudioFreeBuff != 2)&&(retrycnt++ < 10))
		{
			tls_os_time_delay(1);
		}
		if (audio_buffer0)
		{
			memset(audio_buffer0, 0, AUDIO_BUF_SIZE);
			memset(audio_buffer1, 0, AUDIO_BUF_SIZE);	
		}
		//wm_printf("//Fifo len:%d\r\n", FifoDataLen());
#endif	
	}
}

static void I2sMspInit(void)
{
    wm_i2s_ck_config(I2S_BCLK);
    wm_i2s_ws_config(I2S_LRCK);
    wm_i2s_do_config(I2S_DATA_OUT);
    wm_i2s_di_config(I2S_DATA_IN);
#if !CODEC_ALWAYS_AS_MASTER_FTR	
	wm_i2s_extclk_config(I2S_MCLK);
#endif	
}

static void i2sDmaSendCpltCallback(wm_dma_handler_type *hdma)
{
#if USE_WAV_FILE_AS_PLAY_FTR
	if (gAudioFreeBuff == 3)
	{
		playfinishedflag = 1;
	}
	else
#endif		
	{
	    gAudioFreeBuff = 1;
	}
	//wm_printf("tc\r\n");

#if !USE_TASK_FOR_AUDIO
#if USE_MP3_FOR_AUDIO
	dev_ringbuf_out(&dma_ringbuf, NULL, AUDIO_BUF_SIZE);
#else
	AudioCallback(audio_buffer1);
#endif
#endif
}

static void i2sDmaSendHalfCpltCallback(wm_dma_handler_type *hdma)
{
#if USE_WAV_FILE_AS_PLAY_FTR
	if (gAudioFreeBuff == 3)

	{
		playfinishedflag = 1;
	}
	else
#endif	
	{
    	gAudioFreeBuff = 0;
	}
	//wm_printf("th\r\n");

#if !USE_TASK_FOR_AUDIO
#if USE_MP3_FOR_AUDIO
	dev_ringbuf_out(&dma_ringbuf, NULL, AUDIO_BUF_SIZE);
#else
	AudioCallback(audio_buffer0);
#endif
#endif
}

#if USE_MP3_FOR_AUDIO
uint32_t codec_output_write(uint8_t *buf, uint32_t length)
{
	uint32_t ret_len = length;
	
    if (buf == NULL || length == 0) {
        return 0;
    }
	while(ret_len > 0)
	{
		int len = dev_ringbuf_in(&dma_ringbuf, buf, ret_len);
		ret_len -= len;
		buf += len;
		if(ret_len > 0)
		{
			//printf("o");
			tls_os_time_delay(1);
		}
	}
    return length;
}
uint32_t codec_output_write_zero(void)
{
	uint32_t ret_len = dev_ringbuf_avail(&dma_ringbuf);
	uint32_t val = 0;

	while(ret_len >= sizeof(uint32_t))
	{
		dev_ringbuf_in(&dma_ringbuf, &val, sizeof(uint32_t));
		ret_len -= sizeof(uint32_t);
	}
}
#endif

static void i2sDmaRecvCpltCallback(wm_dma_handler_type *hdma)
{
	gAudioFreeBuff = 1;
	//wm_printf("rc\r\n");	
#if !USE_TASK_FOR_AUDIO	
	FifoWrite((uint8_t *)audio_buffer1, AUDIO_BUF_SIZE);
#endif
}
static void i2sDmaRecvHalfCpltCallback(wm_dma_handler_type *hdma)
{
	gAudioFreeBuff = 0;
	//wm_printf("rh\r\n");
#if !USE_TASK_FOR_AUDIO
	FifoWrite((uint8_t *)audio_buffer0, AUDIO_BUF_SIZE);
#endif
}

static void I2sRecordInit(int32_t sr, uint32_t bit_width, int32_t inputSamps, int16_t *buf0, int16_t *buf1)
{
    I2S_InitDef opts;
#if CODEC_ALWAYS_AS_MASTER_FTR	
    opts.I2S_Mode_MS = I2S_MODE_SLAVE;
#else
#if	I2S_PLAY_AS_MASTER_RECORD_AS_SLAVE_FTR
    opts.I2S_Mode_MS = I2S_MODE_SLAVE;
#else
	opts.I2S_Mode_MS = I2S_MODE_MASTER;
#endif
#endif
	opts.I2S_Mode_SS = I2S_CTRL_MONO;
	opts.I2S_Mode_LR = I2S_LEFT_CHANNEL;
	opts.I2S_Trans_STD = I2S_Standard;
	opts.I2S_DataFormat = bit_width;
	opts.I2S_AudioFreq = sr;
#if CODEC_ALWAYS_AS_MASTER_FTR		
	opts.I2S_MclkFreq = 8000000;//256*15 + 256*5/8;
#else
	opts.I2S_MclkFreq = 8000000;
#endif

    wm_i2s_port_init(&opts);

    memset(&dma_rx, 0, sizeof(wm_dma_handler_type));
    dma_rx.XferCpltCallback = i2sDmaRecvCpltCallback;
	dma_rx.XferHalfCpltCallback = i2sDmaRecvHalfCpltCallback;
	wm_i2s_receive_dma(&dma_rx, (uint16_t *)buf0, inputSamps * 2);
}

static void I2sPlayInit(int32_t sr, int32_t nChans, uint32_t bit_width, int32_t outputSamps, int16_t *buf0, int16_t *buf1)
{
    I2S_InitDef opts;
#if CODEC_ALWAYS_AS_MASTER_FTR	
    opts.I2S_Mode_MS = I2S_MODE_SLAVE;
#else	
    opts.I2S_Mode_MS = I2S_MODE_MASTER;
#endif	
	opts.I2S_Mode_SS = I2S_CTRL_MONO;
	opts.I2S_Mode_LR = I2S_LEFT_CHANNEL;
	opts.I2S_Trans_STD = I2S_Standard;
	opts.I2S_DataFormat = bit_width;
	opts.I2S_AudioFreq = sr;
#if 0
	if (sr == 8000)
	{
		opts.I2S_MclkFreq = 256*opts.I2S_AudioFreq*2;
	}
	else
	{
		opts.I2S_MclkFreq = 256*opts.I2S_AudioFreq;
	}
#else
	opts.I2S_MclkFreq = 8000000;
#endif
    wm_i2s_port_init(&opts);

    memset(&dma_tx, 0, sizeof(wm_dma_handler_type));
    dma_tx.XferCpltCallback = i2sDmaSendCpltCallback;
	dma_tx.XferHalfCpltCallback = i2sDmaSendHalfCpltCallback;
	wm_i2s_transmit_dma(&dma_tx, (uint16_t *)buf0, outputSamps * 2);
}
#if 0
void wm_i2s_record_and_play(uint32_t sr, uint8_t bit_width)
{
	/*record*/
	if(sr != 8000 && sr != 16000 && sr != 32000 &&
		sr != 11025 && sr != 22050 && sr != 44100 &&
		sr != 12000 && sr != 24000 && sr != 48000)
	{
		wm_printf("samplerate error!\n");
		return;
	}
	if(bit_width != 8 && bit_width != 16 && bit_width != 24 && bit_width != 32)
	{
		wm_printf("bit_width error!\n");
		return;
	}
	if(sr == 8000 && bit_width == 8)
	{
		wm_printf("not support 8K 8Bit record!\n");
		return;
	}

	FifoClear();
	CodecSetSampleRate(bit_width);
	memset(audio_rbuffer0, 0, AUDIO_BUF_SIZE);
	memset(audio_rbuffer1, 0, AUDIO_BUF_SIZE);		
	{
		I2S_InitDef opts;
		opts.I2S_Mode_MS = I2S_MODE_SLAVE;
		opts.I2S_Mode_SS = I2S_CTRL_MONO;
		opts.I2S_Mode_LR = I2S_LEFT_CHANNEL;
		opts.I2S_Trans_STD = I2S_Standard;
		opts.I2S_DataFormat = bit_width;
		opts.I2S_AudioFreq = sr;

		opts.I2S_MclkFreq = 8000000;//256*15 + 256*5/8;
	
		wm_i2s_port_init(&opts);
	}
	/*record*/
	memset(&dma_rx, 0, sizeof(wm_dma_handler_type));
	dma_rx.XferCpltCallback = i2sDmaRecvCpltCallback;
	dma_rx.XferHalfCpltCallback = i2sDmaRecvHalfCpltCallback;
	/*play*/	
    memset(&dma_tx, 0, sizeof(wm_dma_handler_type));
    dma_tx.XferCpltCallback = i2sDmaSendCpltCallback;
	dma_tx.XferHalfCpltCallback = i2sDmaSendHalfCpltCallback;
	wm_i2s_tranceive_dma(I2S_MODE_SLAVE, &dma_tx, &dma_rx, audio_rbuffer0, audio_buffer0, gsOutputSamps*2);
}
#endif
#if USE_WAV_FILE_AS_PLAY_FTR
void PlayDataSet(uint8_t num)
{
	gplaynum = num;
	playedlen = 0;
}
#endif
#if USE_MP3_FOR_AUDIO
void audio_mp3_decode(void)
{
   int outlen = 0;
   int usedOffset = 0;
   int offset = 0;
   char *out = NULL;
   int chans;
   int samplingRate;
   uint8_t *playdata = NULL;
   int totalplaylen = 0;

   int ret;
   printf("---> %s\r\n", __func__);
   ret = get_audio_resource(gplaynum, playedlen,&playdata,&totalplaylen);
   if (ret < 0)
   {
       printf("@@@no play data\r\n");
       gAudioFreeBuff = 2;
       playfinishedflag = 1;
       return;
   }
   gAudioFreeBuff = 3;	
   while(usedOffset < totalplaylen)
   {
		offset = ad_pvmp3_decode((unsigned char*)playdata, PVMP3DEC_OBUF_SIZE, &out, &outlen, &chans, &samplingRate);

		codec_output_write(out, outlen);
		if(offset <= OUTPUT_BUFFER_TOO_SMALL)
			break;
		playdata += offset;
		usedOffset += offset;
   }

}
#endif

static int32_t AudioCallback(int16_t *samples)
{
	u32 temp = 0;
    static int len = 0;
	int16_t data;
	u32 i = 0;
#if USE_RECORD_DATA_AS_PLAY_FTR
    temp = FifoDataLen();
	//wm_printf("temp:%d\r\n", temp);
	if(temp >= 512)
	{
		UserPACtrl(ENABLE_PA_2_PLAY);
	}
	else
	{
		UserPACtrl(DISABLE_PA_2_STOP_PLAY);
		return -1;
	}
	memset((uint8_t *)samples, 0, AUDIO_BUF_SIZE);
	FifoRead((uint8_t *)samples, AUDIO_BUF_SIZE);
	gAudioFreeBuff = 2;	
#else
#if USE_WAV_FILE_AS_PLAY_FTR
	uint8_t *playdata = NULL;
	int totalplaylen = 0;
	int ret = 0;
	ret = get_audio_resource(gplaynum, playedlen,&playdata,&totalplaylen);
	if (ret < 0)
	{
		printf("@@@no play data\r\n");
		gAudioFreeBuff = 2;
		playfinishedflag = 1;
		return -1;
	}
	
    if(playedlen >= totalplaylen)
    {
        printf("@@@@finish:%d,%d\n");
        playedlen = 0;
		if (gAudioFreeBuff != 3)
		{
			playfinishedflag = 0;
		}
		return -1;
		
    }
	if ((playedlen + AUDIO_BUF_SIZE) >= totalplaylen)
	{
		memcpy((uint8_t *)samples, playdata, totalplaylen - playedlen);
	    playedlen = totalplaylen;	
		gAudioFreeBuff = 3;
		printf("last play data:%d\n", playedlen);
		return 0;
	}
	else
	{
		memcpy((uint8_t *)samples, playdata, AUDIO_BUF_SIZE);
	    playedlen += AUDIO_BUF_SIZE;
		gAudioFreeBuff = 2;
		//printf("not last play data:%d\n", playedlen);
		return 0;
	}

#else
    if((len + AUDIO_BUF_SIZE) > (3 * 70 * 1024))
    {
        printf("finish\n");
        len = 0;
    }
    tls_fls_read(0x8010000 + len, (uint8_t *)samples, AUDIO_BUF_SIZE);
    len += AUDIO_BUF_SIZE;
	gAudioFreeBuff = 2;
#endif
#endif
	return 0;
}
#if USE_TASK_FOR_AUDIO
static void mp3play_localdata(void *data)
{    
	for(;;)
	{
	    if(gMode == gMode_PLAY_START)
	    {
    		if(0 == gAudioFreeBuff)
    		{
    			AudioCallback(audio_buffer0);
    		}
    		else if(1 == gAudioFreeBuff)
    		{
    			AudioCallback(audio_buffer1);
    		}
	    }
        else if(gMode == gMode_RECORD_START)
        {
            if(0 == gAudioFreeBuff)
    		{
    			FifoWrite((uint8_t *)audio_buffer0, AUDIO_BUF_SIZE);
                gAudioFreeBuff = 2;
    		}
    		else if(1 == gAudioFreeBuff)
    		{
    			FifoWrite((uint8_t *)audio_buffer1, AUDIO_BUF_SIZE);
                gAudioFreeBuff = 2;
    		}
        }
        
	    tls_os_time_delay(2);
	}
}
#endif

void AudioPlayInit(void)
{
#if USE_MALLOC_AUDIO_BUF_FTR
	audio_buffer0 = MEM_MALLOC(AUDIO_BUF_SIZE * 2);
	if (NULL == audio_buffer0)
	{
		printf("audio buffer not enough!!!\r\n");
		return;
	}
#if USE_MP3_FOR_AUDIO	
	dev_ringbuff_init(&dma_ringbuf, (uint8_t *)audio_buffer0, AUDIO_BUF_SIZE*2);	
#endif
	memset((void *)audio_buffer0, 0, AUDIO_BUF_SIZE*2);
	audio_buffer1 = audio_buffer0 + AUDIO_BUF_SIZE/2;
#if 0
	audio_rbuffer0 = MEM_MALLOC(AUDIO_BUF_SIZE * 2);
	if (NULL == audio_rbuffer0)
	{
		printf("audio buffer not enough!!!\r\n");
		return;
	}
	memset((void *)audio_rbuffer0, 0, AUDIO_BUF_SIZE*2);
	audio_rbuffer1 = audio_rbuffer0 + AUDIO_BUF_SIZE/2;
#endif	
#if USE_TASK_FOR_AUDIO	
	taskmp3stk = MEM_MALLOC(TASK_MP3_STK_SIZE * sizeof(u32));
	if (NULL == taskmp3stk)
	{
		printf("taskmp3stk buffer not enough!!!\r\n");
		MEM_FREE(audio_buffer0);
		audio_buffer0 = NULL;
		audio_buffer1 = NULL;
#if 0		
		MEM_FREE(audio_rbuffer0);		
		audio_rbuffer0 = NULL;
		audio_rbuffer1 = NULL;
#endif		
		return;
	}
#endif	
#else
	memset((void *)audio_buffer0, 0, AUDIO_BUF_SIZE);
	memset((void *)audio_buffer1, 0, AUDIO_BUF_SIZE);
#endif	
	CodecMCLKOut(5);
	I2sMspInit();
    CodecInit();
#if USE_TASK_FOR_AUDIO
	tls_os_task_create(NULL, NULL,
	                    mp3play_localdata,
	                    (void *)0,
	                    (void *)taskmp3stk,          /* 任务栈的起始地址 */
	                    TASK_MP3_STK_SIZE * sizeof(u32), /* 任务栈的大小     */
	                    TASK_MP3_PRO,
	                    0);
	
#endif	
}

void AudioPlayDeInit(void)
{
	RecordStop();
	PlayStop();
	if (audio_buffer0)
	{
		MEM_FREE(audio_buffer0);
		audio_buffer0 = NULL;
		audio_buffer1 = NULL;
	}
}


void AudioVolumeSet(uint8_t vol)
{
	gVolume = vol;
}

