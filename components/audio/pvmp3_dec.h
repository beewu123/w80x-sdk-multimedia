#ifndef _PVMP3_DEC_H_
#define _PVMP3_DEC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include <stdbool.h>

#include "pvmp3decoder_api.h"


#define PVMP3DEC_OBUF_SIZE     (1024 * 12)
// #define PVMP3DEC_OBUF_SIZE     (1024 * 2)

struct ad_pvmp3_priv {
    void                     *obuf;  // buf for output
    void                     *dbuf;  // buf for decoder
    tPVMP3DecoderExternal    config;
};

int ad_pvmp3_open(void);
int ad_pvmp3_decode(unsigned char* data, int len, unsigned char **out, int *outlen, int *chans, int *samplingRate);
int ad_pvmp3_close(void);


#ifdef __cplusplus
}
#endif

#endif /* _PVMP3_DEC_H_ */


