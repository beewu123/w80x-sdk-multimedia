#ifndef __USER_TASK_H__
#define __USER_TASK_H__
#include "wm_include.h"
#include <string.h>
#include "audio.h"


#define USER_DEBUG						1
#if USER_DEBUG
#define USER_PRINT wm_printf
#else
#define USER_PRINT(fmt, ...)
#endif
#define USER_FW_VER						"1.0.2"





void VoiceUserMain(void);

#endif
