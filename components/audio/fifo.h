#ifndef _FIF0_H_
#define _FIFO_H_

#define TRANTISION_BUFF_SIZE	2048

#if 1
typedef struct fifo_t {
    u8 *buf;
	u32 size;
	u32 in;
	u32 out;
} _fifo_str;

#define min(x,y) ((x) < (y)?(x):(y))
#else
typedef struct fifo_t {
    u8 *buf;
    u8 *head;
    u8 *tail;
    u32 size;
} _fifo_str;
#endif

int FifoInit(uint8_t *fifo_addr, uint32_t fifo_size);

int FifoDataLen(void);

int FifoSpaceLen(void);

int FifoRead(uint8_t *buf, uint32_t len);

int FifoWrite(uint8_t *buf, uint32_t len);

void FifoClear(void);

int FifoGetRemainLen(void);

void FifoRemoveData(uint32_t len);

u8 * FifoGetCurTail(void);

#endif

