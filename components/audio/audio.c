/***********************************************************************
*	INCLUDE FILES
***********************************************************************/
//#include "../user_config.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "wm_include.h"
#include "wm_mem.h"
#include "fifo.h"
#include "audioplay.h"
#include "audio.h"

// ���뻺��
static uint16_t *audio_fifo = NULL;

int RecordFifoInit(uint32_t fifo_size)
{
	if (audio_fifo)
	{
		return 0;
	}

    if(fifo_size < (2*AUDIO_BUF_SIZE))
    {
        fifo_size = 2*AUDIO_BUF_SIZE;
    }
	audio_fifo = (uint16_t *)tls_mem_alloc(fifo_size);
	if(audio_fifo == NULL)
	{
		wm_printf("AudioInit malloc failed\r\n");
		return 1;
	}
	FifoInit((uint8_t *)audio_fifo, fifo_size);
	return 0;
}


void RecordFifoDeInit(void)
{
	if (audio_fifo)
	{
	   FifoClear();
	   tls_mem_free(audio_fifo);
	   audio_fifo = NULL;
	}
}

/*************************************************************************** 
* Description: AudioInit
*
* Auth: XY
*
*Date: 2016-7-15
****************************************************************************/ 
uint8_t AudioInit(uint32_t fifo_size)
{
	AudioPlayInit();
	return 0;
}

uint8_t AudioDeinit(void)
{
	AudioPlayDeInit();
	RecordFifoDeInit();
	return 0;
}



