#ifndef __LV_PORT_TICK_H__
#define __LV_PORT_TICK_H__
#include "lvgl.h"

#define LV_TICK_PERIOD_MS 1

void lv_create_tick(void);

#endif
