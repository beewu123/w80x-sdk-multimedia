/***************************************************************************** 
* 
* File Name : main.c
* 
* Description: main 
* 
* Copyright (c) 2014 Winner Micro Electronic Design Co., Ltd. 
* All rights reserved. 
* 
* Author : houxf
* 
* Date : 2021-12-1
*****************************************************************************/ 
#include "wm_include.h"
#include "psram.h"
#include "camera_config.h"
#include "ui.h"
#include "../user_config.h"

#if USE_PSRAM_CAMERA
#define MEM_MALLOC  dram_heap_malloc
#define MEM_FREE    dram_heap_free
#else
#define MEM_MALLOC  tls_mem_alloc
#define MEM_FREE    tls_mem_free
#endif

// task 
tls_os_queue_t *gsCameraTaskQueue = NULL;
static OS_STK CameraTaskStk[CAMERA_TASK_SIZE]; 
static char gsDataBuff[256];
static int CreateCameraTask(void);
static void CameraTaskProc(void* param);
static void CameraSkTxData(void);

// 创建LVGL图像描述符
static lv_img_dsc_t img_dsc;
// 图像缓存buf
static u8 *image_buf = NULL;

static bool cap_loop = 0;

/*************************************************************************** 
* Description: CameraDeviceInit
*
* Auth: houxf
*
*Date: 2016-12-11
****************************************************************************/ 
void CameraTaskInit(void)
{
	memset(gsDataBuff, 0, sizeof(gsDataBuff));
//用户自己的task
	CreateCameraTask();
}

static int CreateCameraTask(void)
{
	tls_os_queue_create(&gsCameraTaskQueue, CAMERA_QUEUE_SIZE);
	tls_os_task_create(NULL, "camera", CameraTaskProc, NULL,
                    (void *)CameraTaskStk,         			 /* 任务栈的起始地址 */
                    CAMERA_TASK_SIZE * sizeof(u32),		 /* 任务栈的大小     */
                    CAMERA_TASK_PRO,
                    0);
	return WM_SUCCESS;
}
/*************************************************************************** 
* Description: 用户任务处理回调函数
*
* Auth: houxf
*
*Date: 2016-12-11
****************************************************************************/ 
static void CameraTaskProc(void* param)
{
	void *msg;
	CameraPinDef pin;
//	Camera Init 
	pin.camera_addr = CAMERA_ADDR;
	pin.scl = CAMERA_I2C_SCL;
	pin.sda = CAMERA_I2C_SDA;
	pin.pdwn = CAMERA_PWDN;
	pin.mclk = CAMERA_MCLK;
	pin.cs = CAMERA_SPI_CS;
	pin.clk = CAMERA_SPI_CLK;
	pin.d0 = CAMERA_SPI_D0;
	pin.d1 = CAMERA_SPI_D1;
	CameraInit(CAP_IMG_QVGA_RGB565, pin);
	CameraSetMirrorFlip(0,0);
    image_buf = MEM_MALLOC(320 * 240 * 2);
    if(image_buf == NULL)
    {
        printf("---> camera buf malloc fail!\n");
        return;
    }
    // LVGL图像描述符
    img_dsc.header.always_zero = 0;
    img_dsc.header.w = 320;
    img_dsc.header.h = 240;
    img_dsc.data_size = 320*240*2;
    img_dsc.header.cf = LV_IMG_CF_TRUE_COLOR;
    img_dsc.data = image_buf;

	for(;;) 
	{
		tls_os_queue_receive(gsCameraTaskQueue, (void **)&msg, 0, 0);
		switch((u32)msg)
		{
			case MSG_SCREEN_LOADED:
				USER_PRINT("---> MSG_SCREEN_LOADED\r\n");
                cap_loop = 1;
                tls_os_queue_send(gsCameraTaskQueue, (void *)MSG_CAMERA_CAP, 0);
				break;

			case MSG_SCREEN_UNLOADED:
				USER_PRINT("---> MSG_SCREEN_UNLOADED\r\n");
                cap_loop = 0;
				break;	
				
			case MSG_CAMERA_CAP:
				CameraSkTxData();
				break;
				
			default:
				break;
		}
	}
}


static void CameraSkTxData(void)
{	
    // u32 t = tls_os_get_time();
	// printf("--->%d %s\r\n", __LINE__, __func__);
	CaptureImage(image_buf);
    // printf("---> time: %d\n", (tls_os_get_time() - t));

    // 设置图像数据
    lv_img_set_src(ui_CamFrame, &img_dsc);
    tls_os_time_delay(1);

    if(cap_loop){
        tls_os_queue_send(gsCameraTaskQueue, (void *)MSG_CAMERA_CAP, 0);
    }
}

