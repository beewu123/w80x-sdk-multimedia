#ifndef __CAMERA_CONFIG_H__
#define __CAMERA_CONFIG_H__
#include "wm_include.h"
#include "wm_psram.h"
#include "libcamera.h"
#include "../user_config.h"


#define USER_DEBUG		1
#if USER_DEBUG
#define USER_PRINT printf
#else
#define USER_PRINT(fmt, ...)
#endif


#define CAMERA_TASK_SIZE					1024	// 用户任务栈的size
#define CAMERA_TASK_PRO					    55		// 用户任务优先级

#define CAMERA_QUEUE_SIZE				    4		// 用户消息队列的size
#define MSG_SCREEN_LOADED					0x01	// 
#define MSG_SCREEN_UNLOADED					0x02	// 
#define MSG_CAMERA_CAP						0x03	

// min 2ms 
#define USER_DELAY_TICK(time) 			tls_os_time_delay(time)
#define USER_TIME_GET					tls_os_get_time	

extern void tls_sys_reset(void);

#endif
