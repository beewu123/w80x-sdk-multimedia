#ifndef __CAMERA_H__
#define __CAMERA_H__
#include "wm_pwm.h"
#include "wm_gpio_afsel.h"
#include <string.h>
#include "wm_psram.h"
#include "wm_hspi.h"

#define CAMERA_PWDN		WM_IO_PB_29
#define CAMERA_MCLK		WM_IO_PB_13

#define CAMERA_SPI_CS	WM_IO_PB_14
#define CAMERA_SPI_CLK	WM_IO_PB_12
#define CAMERA_SPI_D0	WM_IO_PB_15
#define CAMERA_SPI_D1	WM_IO_PB_16

#define CAMERA_I2C_SCL  WM_IO_PB_18
#define CAMERA_I2C_SDA  WM_IO_PB_17

#define CAMERA_ADDR		0x42

typedef enum
{
	CAP_IMG_VGA_YUV422 = 1, // 640 * 480 YUV422
	CAP_IMG_QVGA_YUV422,	// 320 * 240 YUV422
	CAP_IMG_VGA_Y,			// 640 * 480 only y
	CAP_IMG_QVGA_Y,			// 320 * 240 only y
	CAP_IMG_VGA_RGB565,		// 640 * 480 RGB565
	CAP_IMG_QVGA_RGB565,	// 320 * 240 RGB565
}resolution_type;

typedef struct
{
	enum tls_io_name scl;
	enum tls_io_name sda;
	enum tls_io_name pdwn;
	enum tls_io_name mclk;
	enum tls_io_name cs;
	enum tls_io_name clk;
	enum tls_io_name d0;
	enum tls_io_name d1;
	uint8_t camera_addr;
}CameraPinDef;

void CameraInit(resolution_type type, CameraPinDef pin);
uint8_t CaptureImage(uint8_t *pbuff);
uint8_t CapturePartImage(uint8_t *pbuff, uint16_t start_x, uint16_t start_y, uint16_t width, uint16_t height);
void CameraSetMirrorFlip(uint8_t mirror_en, uint8_t flip_en);
void CameraSetExposure(uint8_t auto_en, uint16_t value);


#endif
